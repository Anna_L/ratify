/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


/* This model defines the data type "Song" */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//define Schema for song
var songSchema = new Schema({
    title: String,
    artist: String,
    duration: String,
    trackId: String,
    playlistTitle: String,
    positionInPlaylist: Number,
    colorTag: String,
    heartRating: [{
        rating: String,
        sessionId: String
    }],
    starRating: [{
        rating: String,
        sessionId: String
    }],
    thumpUpRating: [{
        rating: String,
        sessionId: String
    }],
    sunRating: [{
        rating: String,
        sessionId: String
    }],
    happyRating: [{
        rating: String,
        sessionId: String
    }]

}, {
    collection: 'songs'
}); //all songs will be saved to the collection 'songs'

var Song = mongoose.model('songs', songSchema);

module.exports = Song;
