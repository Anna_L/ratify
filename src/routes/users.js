/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var User = mongoose.model('user');

/* GET users page. */
router.get('/', isLoggedIn, function(req, res, next) {
  User.find(function(err, data) {
    console.log(err);
    console.log(data);
    //create one array with all usernames and ids that will be displayed in the table
    var allusers = new Array();
    for (var i = 0; i < data.length; i++) {
      allusers.push({
        username: data[i].name,
        userid: data[i]._id
      });
    }
    res.render('users', {
      title: 'Users',
      text: data,
      allusers: allusers
    });
  });
});

/* POST */
//delete user by id
router.post('/delete', function(req, res, next) {
  if (!req.body || !req.body.id) {
    return next(new Error('No data provided.'));
  } else {
    User.findById(req.body.id, function(err, user) {
      if (err || !user) {
        return next(new Error('Not Found.'));
      } else {
        user.remove(function(err) {
          if (err) {
            return next(new Error('Something went wrong.'));
          } else {
            console.log(user._id + ' deleted');
            res.redirect('/users');
          }
        });
      }
    });
  }
});

//update user by id
router.post('/update', function(req, res, next) {
  if (!req.body || !req.body.id || !req.body.name) {
    return next(new Error('Not all data provided.'));
  } else {
    User.findById(req.body.id, function(err, user) {
      if (err || !user) {
        return next(new Error('Not Found.'));
      } else {
        user.name = req.body.name; //set new name
        user.save(); //save changed user to database
        console.log(user._id + 'updated');
        res.redirect('/users');
      }
    });
  }
});

//route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated()){
        return next();
    }

    // if user is not authenticated, redirect to the sign-in page
    res.redirect('/signin');
}

module.exports = router;
