/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


var express = require('express');
var router = express.Router();
var passport = require('passport');

/* GET signup page. */
router.get('/', isLoggedIn, function(req, res, next) {
	res.render('signup', {
    	title: 'Sign Up',
        error: req.flash('error')
  	});
});

/* Do Sign Up */
router.post('/dosignup',
	passport.authenticate('local-signup', {
     	successRedirect : '/users',
    	failureRedirect : '/signup',
        failureFlash : true
    })
);

//route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated()){
        return next();
    }

    // if user is not authenticated, redirect to the sign-in page
    res.redirect('/signin');
}

module.exports = router;
