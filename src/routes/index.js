/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


var express = require('express');
var router = express.Router();

var mongoose = require('mongoose');
var Song = mongoose.model('songs');
var Playlist = mongoose.model('playlists');

var utils = require("../lib/utils");

var player_trackId;

/* GET all Playlists in JSON. */
router.get('/getallplaylists', function(req, res, next) {
  Playlist.find(function(err, data) {
    console.log(err);
    console.log(data);

    res.json(data);
  });
});

/* GET all Songs in JSON. */
router.get('/getallsongs', function(req, res, next) {
  Song.find(function(err, data) {
    console.log(err);
    console.log(data);

    allsongs = utils.prepare_song_array(data);
    res.json(allsongs);
  });
});

/* GET index page. */
router.get('/', function(req, res, next) {
  Song.find(function(err, data) {
    console.log(err);
    console.log(data);

    sessionId = req.session.id; //set sessionID
    allsongs = utils.prepare_song_array(data);
    Playlist.find(function(err, data) {
      console.log(err);
      var playlists = data;
      res.render('index', {
        title: 'Ratify',
        allsongs: allsongs,
        trackId: player_trackId,
        playlists: playlists
      });
    });
  });
});

//delete song by id
router.post('/delete', function(req, res, next) {
  if (!req.body || !req.body.id) {
    return next(new Error('No data provided.'));
  } else {
    Song.findById(req.body.id, function(err, song) {
      if (err || !song) {
        return next(new Error('Not Found.'));
      } else {
        song.remove(function(err) {
          if (err) {
            return next(new Error('Something went wrong.'));
          } else {
            console.log(song._id + ' deleted');
            res.redirect('/');
          }
        });
      }
    });
  }
});

//rate song
router.post('/rate', function(req, res, next) {
  if (!req.body || !req.body.objectId || !req.body.rating) {
    return next(new Error('No data provided.'));
  } else {
    Song.findById(req.body.objectId, function(err, song) {
      if (err || !song) {
        return next(new Error('Track not found.'));
      } else {
        song.rating = req.body.rating;
        song.save();
        res.redirect('/');
      }
    });
  }
});

module.exports = router;