/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */

module.exports = {
    db: {
        // We are using mongoose, please check http://mongoosejs.com/docs/connections.html
        // on how to set use user authentication, etc.
        production: "mongodb://localhost/playlisteval",
        test: "mongodb://localhost/playlisteval_test"
    },

    server_config: {
        port: 3000
    }
};