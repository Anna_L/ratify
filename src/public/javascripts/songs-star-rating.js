/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


/* Script for star ratings of songs */

$(document).ready(function() {
  $('.star-rating').rating({
    'showCaption': false,
    'stars': '5',
    'min': '0',
    'max': '5',
    'step': '1',
    'size': 'xs',
    'starCaptions': {
      0: 'Not Rated',
      1: 'Very Poor',
      2: 'Poor',
      3: 'Ok',
      4: 'Good',
      5: 'Very Good'
    }
  });
});
