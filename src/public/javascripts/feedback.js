/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */


// Function to submit the feedback of Ratify to the server's socket.
function submitRatifyFeedbackToSocket() {
  var socket = io();
  socket.emit('submitRatifyFeedback', $('#feedbackInput').val());
  // Display success message after the user's feedback.
  socket.on('thankYouForFeedback', function() {
    $('#thankYouForFeedback').attr('class', 'alert alert-success');
    $('#thankYouForFeedback').text("Thank you for your Feedback!");
  });
  // Delete success message after 2 seconds.
  setTimeout(function() {
    $('#thankYouForFeedback').attr('class', '');
    $('#thankYouForFeedback').text("");
  }, 2000)
}
