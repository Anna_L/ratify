var playlistUserApp = angular.module('playlistUserApp', []);
var myModule = angular.module('myModule', ['ng-polymer-elements']);

playlistUserApp.controller('PlaylistUserCtrl', function($scope, $http) {
  $http.get("/getallplaylists")
    .then(function(response) {
      $scope.playlists = response;
    });
  $http.get("/getallsongs")
    .then(function(response) {
      $scope.allsongs = response;
    });

  // Function to return all songs in JSON format.
  $scope.getAllsongs = function() {
    return $scope.allsongs;
  }

  // Function to load a Ratify playlist to the player and display it in the table.
  $scope.loadRatifyPlaylist = function() {
    var selectedPlaylistJSON = JSON.parse($scope.selectedPlaylist);
    $scope.showedPlaylist = selectedPlaylistJSON;
    playerLoadRatifyPlaylist($scope.showedPlaylist.playlistId);
    // Set timeout because otherwise the player hasn't loaded the playlist completely.
    setTimeout(function() {
      getPlaylistDuration();
    }, 300)
  }

  // Function to set the rating of the actual selected song.
  $scope.setRating = function(rating) {
    $scope.rating = rating;
  }

  // Function to submit the rating of a playlist to the server's socket.
  $scope.submitPlaylistRatingToSocket = function(ratingType) {
    var socket = io();
    socket.emit('ratePlaylist', $scope.rating, $scope.showedPlaylist._id, ratingType);
  }

  // Function to submit the rating of a song to the server's socket.
  $scope.submitSongRatingToSocket = function(table_objectId, ratingType) {
    var socket = io();
    socket.emit('rateSong', $scope.rating, table_objectId, ratingType);
  }
});
