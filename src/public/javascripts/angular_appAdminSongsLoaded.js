var angularAdminSongsLoaded = angular.module('angularAdminSongsLoaded', []);

angularAdminSongsLoaded.controller('angularSongsLoadedCtrl', function($scope, $http) {
  $http.get("/getallplaylists")
    .then(function(response) {
      $scope.existingPlaylists = response;
    });
  $scope.playlist = [];
  $scope.songIDs = [];
  $scope.notImportedSongs = [];
  $scope.addSongToPlaylistTable = function(nr, title, duration, artist) {
    $scope.playlist.push({
      nr: nr,
      title: title,
      duration: duration,
      artist: artist
    });
  }

  // Function to reset the not imported songs array.
  $scope.resetNotImportedSongsArray = function() {
    $scope.notImportedSongs = [];
  }

  // Function to clear the displayed playlist.
  $scope.resetPlaylistTable = function() {
    $scope.playlist = [];
  }

  // Function to reset the imported song IDs-array.
  $scope.resetSongIDsArray = function() {
    $scope.songIDs = [];
  }

  // Function to get the ID of the selected playlist.
  $scope.getSelectedPlaylistID = function() {
    return $scope.selectedPlaylistID;
  }

  // Function to re-load the existing playlists from the database.
  $scope.reloadDisplayedPlaylists = function() {
    $http.get("/getallplaylists")
      .then(function(response) {
        $scope.existingPlaylists = response;
      });
  }
});
