/*!
 * ratify
 * Copyright(c) 2016 Stephan Fremerey
 * Copyright(c) 2016 Anna Linke
 * Copyright(c) 2016 Julian Zebelein
 * MIT Licensed
 */





var config = require("../src/config");
var mongoose = require("mongoose");

var playlist_db = config.db["test"];
mongoose.connect(playlist_db);

module.exports = mongoose;
